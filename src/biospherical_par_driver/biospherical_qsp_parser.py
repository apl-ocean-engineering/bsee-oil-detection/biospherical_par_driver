from typing import Optional
import rospy

import biospherical_par_msgs


def parse_qsp_bytes(data: bytes) -> Optional[biospherical_par_msgs.msg.PARIrradiance]:
    """
    String received from the Biospherical QSP-2150 PAR is a two-field CSV,
    ASCII, utf-8 encoded into raw bytes, with an end-of-line
    terminator of CR-LF.

    The first field is scalar irradiance in uE cm^-2 s^-1

    Example (from data sheet):

    .013016, 16.06
    .012990, 16.06


    If parsing fails, returns None.
    Does not raise any exceptions

    If parsing succeeds, returns PARIrradiance message.
    Header will be uninitialized.

    ""

    """
    try:
        # We want decode errors to raise a UnicodeDecodeError, rather than trying
        # to recover and potentially introducing corrupt data.
        data_str = data.decode("utf-8", errors="strict")
    except Exception as ex:
        # I think UnicodeDecodeError is the only exception decode raises, but I'm not
        # positive about that, so go ahead and catch anything.
        rospy.logerr(
            "{} \n Unable to decode bytes into utf-8! {}".format(ex, list(data))
        )
        return None

    tokens = data_str.split(",")
    num_fields = 3  # magic number -- expected number of fields in a message.
    if len(tokens) != num_fields:
        rospy.logerr(
            "Incorrect number of fields in data. expected {}, got {}: input = {}. ".format(
                num_fields, len(tokens), data_str
            )
        )
        return None

    # From the datasheet:
    # Typical output data stream:  Displayed are irradiance in uE^-2 m^-2 sec^-1,
    # temperature in deg C and line voltage in volts.
    irrad, tempc, line_voltage = tokens

    msg = biospherical_par_msgs.msg.PARIrradiance()

    try:
        msg.irradiance = float(irrad)
        msg.temperature = float(tempc)
    except Exception as ex:
        rospy.logerr("{} \n Unable to decode values: {}".format(ex, tokens))
        return None

    return msg
