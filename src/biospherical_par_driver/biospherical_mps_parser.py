from typing import Optional
import rospy

import biospherical_par_msgs


def parse_mps_bytes(data: bytes) -> Optional[biospherical_par_msgs.msg.PARIrradiance]:
    """
    The data output from the MPS-PAR is highly configurable,
    however for now we'll assumed the default (which is also what
    their LightLogger software expects):

    MPS	PAR,000604,879.26005125,1552.36016958,uE/(cm2.s),01/17/24,2.4853,12,125,200.1835,198.6333,08389068,08389303,08389305<cr><lf>
    <AY,-0.0000711,28.5<cr><lf>
    <AY,-0.0000651,28.5<cr><lf>

    The first is a power-on header line which we report by ignore.

    On the following data lines:
        The first field is a constant (but reconfigurable) header
        The second field is scalar irradiance in uE cm^-2 s^-1
        The third field is instrument temperature in C

    If parsing fails, returns None.
    Does not raise any exceptions

    If parsing succeeds, returns PARIrradiance message.
    Header will be uninitialized.

    ""

    """
    try:
        # We want decode errors to raise a UnicodeDecodeError, rather than trying
        # to recover and potentially introducing corrupt data.
        data_str = data.decode("utf-8", errors="strict")
    except Exception as ex:
        # I think UnicodeDecodeError is the only exception decode raises, but I'm not
        # positive about that, so go ahead and catch anything.
        rospy.logerr(f"{ex} \n Unable to decode bytes into utf-8! {list(data)}")
        return None

    # Catch startup strings
    if data_str.startswith("MPS PAR") and len(data_str) > 0:
        rospy.loginfo(f"MPS_PAR header: {data_str}")
        return None

    tokens = data_str.split(",")
    num_fields = 3  # magic number -- expected number of fields in a message.
    if len(tokens) != num_fields:
        rospy.logerr(
            f"Incorrect number of fields in data. expected {num_fields}, got {len(tokens)}: input = {data_str}. "
        )
        return None

    # From the datasheet:
    # Typical output data stream:  Displayed are irradiance in uE^-2 m^-2 sec^-1,
    # temperature in deg C and line voltage in volts.
    _header, irrad, tempc = tokens

    msg = biospherical_par_msgs.msg.PARIrradiance()

    try:
        msg.irradiance = float(irrad)
        msg.temperature = float(tempc)
    except Exception as ex:
        rospy.logerr(f"{ex} \n Unable to decode values: {tokens}")
        return None

    return msg
