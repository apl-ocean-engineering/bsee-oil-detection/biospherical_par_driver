#! /usr/bin/env python3
"""
Driver specialized for the firmware on Craig McNeil's ProOceanus MiniTDGP
sensors. (They have the configuration menu disabled, are configured to 9600
baud, and provide additional data beyond the usual fields.)
"""

import rospy

import apl_msgs.msg
import biospherical_par_msgs.msg
import serial

from typing import Callable, Optional

from biospherical_par_driver.biospherical_qsp_parser import parse_qsp_bytes
from biospherical_par_driver.biospherical_mps_parser import parse_mps_bytes


class BiosphericalParDriver:
    def __init__(
        self,
        parser_func: Callable[
            [bytes], Optional[biospherical_par_msgs.msg.PARIrradiance]
        ],
    ) -> None:
        rospy.logdebug("BiosphericalParDriver.__init__")
        self.setup_parameters()

        self.raw_pub = rospy.Publisher("~raw", apl_msgs.msg.RawData, queue_size=1)
        self.par_pub = rospy.Publisher(
            "~par", biospherical_par_msgs.msg.PARIrradiance, queue_size=1
        )

        self.parser = parser_func

        baudrate = 9600
        serial_timeout = 2.0  # seconds
        self.serial = serial.Serial(
            port=self.port, baudrate=baudrate, timeout=serial_timeout
        )

        self.termination = bytes([0x0D, 0x0A])

    def setup_parameters(self) -> None:
        self.port = rospy.get_param("~port")
        self.frame_id = rospy.get_param("~frame_id")

    def publish_raw(self, data: bytes, stamp: rospy.Time, frame_id: str) -> None:
        """Publish received data to the raw topic"""
        raw_msg = apl_msgs.msg.RawData()
        raw_msg.header.stamp = stamp
        raw_msg.header.frame_id = frame_id
        raw_msg.direction = raw_msg.DATA_IN
        raw_msg.data = data
        self.raw_pub.publish(raw_msg)

    def run(self) -> None:
        # First, clear stale data
        data = self.serial.read_all()
        # I'm surprised that mypy  made me check for `is not None` here; I would have
        # expected `read_all` to have the same return type as `read_until`
        if data is not None and len(data) > 0:
            rospy.loginfo("Cleared {} bytes of data".format(len(data)))

        while not rospy.is_shutdown():
            try:
                data = self.serial.read_until(self.termination)
                data_stamp = rospy.Time.now()
                if len(data) > 0:
                    self.publish_raw(data, data_stamp, self.frame_id)

                    msg = self.parser(data)
                    if msg is not None:
                        msg.header.stamp = data_stamp
                        msg.header.frame_id = self.frame_id
                        self.par_pub.publish(msg)

            except Exception as ex:
                rospy.logerr("Unhandled Exception! {}".format(ex))


def qsp_main() -> None:
    rospy.init_node("biospherical_par_driver_QSP-2150_sensor")
    par = BiosphericalParDriver(parse_qsp_bytes)
    par.run()


def mps_main() -> None:
    rospy.init_node("biospherical_par_driver_MPS-PAR_sensor")
    par = BiosphericalParDriver(parse_mps_bytes)
    par.run()
