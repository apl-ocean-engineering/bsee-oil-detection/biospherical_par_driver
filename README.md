## Biospherical PAR driver

ROS driver for [Biospherical Instruments](http://www.biospherical.com/) Photosynthetically Active Radiation (PAR) sensors.  We have used the QSP-2150 and MPS-PAR models.  This driver will support both units, although we invested little effort in customizing the units; the drivers reflect the default communications settings and protocol of the units out of the box.

Both models instruments are configured to run at 9600 baud and simply
spit data out on the serial port, no automated configuration needed OR desired.

The two instruments are served by different drivers:

```
rosrun biospherical_par_driver biospherical_mps_par_driver
rosrun biospherical_par_driver biospherical_qsp_par_driver
```

### testing

For now, testing is manual:

`catkin test biospherical_par_driver`

Or, for test coverage:
~~~python
coverage run test/test_parsing.py
coverage html
firefox htmlcov/index.html
~~~

TODO: Figure out
* run tests in Gitlab CI
* Confirm 100% test coverage in Gitlab CI
* rostest that tests the ROS node initialization
