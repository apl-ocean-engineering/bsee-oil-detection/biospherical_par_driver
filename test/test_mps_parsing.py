import unittest
import numpy as np

from biospherical_par_driver import parse_mps_bytes
import biospherical_par_msgs


class TestParsing(unittest.TestCase):
    # Example of valid string from Craig's documentation
    docs_example = "AY,-0.0000711,28.5\r\n".encode("utf-8")

    # Example of valid string from my playing with the instrument
    real_example = "AY,-0.0000711,28.5\r\n".encode("utf-8")

    def test_good_input(self):
        for input in [self.docs_example, self.real_example]:
            msg = parse_mps_bytes(input)
            self.assertIsInstance(msg, biospherical_par_msgs.msg.PARIrradiance)

    def test_invalid_bytes(self):
        """
        0x9c, 0xa5 Are both examples of invalid unicode bytes

        Insert an undecodeable byte at a random position of the input, confirm
        that the parser catches the error.
        """
        for _ in np.arange(5):
            bad_bytearray = bytearray(self.docs_example)
            idx = np.random.randint(len(bad_bytearray))
            bad_bytearray[idx] = 0x9C
            bad_input = bytes(bad_bytearray)
            msg = parse_mps_bytes(bad_input)
            self.assertIsNone(msg)

    def test_too_few_tokens(self):
        # simply removed single field from docs_example
        input_data = ".013016\r\n".encode("utf-8")
        msg = parse_mps_bytes(input_data)
        self.assertIsNone(msg)

    def test_too_many_tokens(self):
        # repeated field in docs_example
        input_data = ".013016, 16.06, 14.3, 15.6\r\n".encode("utf-8")
        msg = parse_mps_bytes(input_data)
        self.assertIsNone(msg)

    def test_wrong_types(self):
        # Turned an int into a float in docs_example

        input_data = ".013016, ABC\r\n".encode("utf-8")
        msg = parse_mps_bytes(input_data)
        self.assertIsNone(msg)


if __name__ == "__main__":
    unittest.main()
